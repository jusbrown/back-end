﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    public class Part
    {
        public bool Qpl { get; set; }
        public string PartNumber { get; set; }
        public string PartName { get; set; }
        public string Revision { get; set; }
        public string ToolDieSetNumber { get; set; }
        public bool OpenPo { get; set; }
        public string Customer { get; set; }
        public string Supplier { get; set; }
        public string SupplierCodes { get; set; }
        public string Jurisdiction { get; set; }
        public string Classification { get; set; }
        public bool Ctq { get; set; }
        public int QplCreatedByUserId { get; set; }
        public DateTime QplCreatedDate { get; set; }
        public int QplLastUpdatedByUserId { get; set; }
        public DateTime QplLastUpdatedDate { get; set; }
        public DateTime? QplExpiresDate { get; set; }
    }
}