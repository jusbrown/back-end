﻿using Quality.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Quality.Web.Repository
{
    public class PartRepository : IPartRepository
    {
        public List<Part> Get(int page, int size)
        {
            var partList = new List<Part>();
            var dataTable = new DataTable();

            //Connect to database and read results into dataTable
            //Would normally pull the connection string from Config file
            using (var conn = new SqlConnection("Server=DESKTOP-QKMBUA7;Database=Quality.Database;Trusted_Connection=True"))
            {
                //Use resource with sql file, so I can run sql directly against database,
                //normally I would have used Entity Framework or Stored Procedure
                using (var com = new SqlCommand(
                    SqlResource.PartList, conn))
                {
                    //Pass in page and size for paging
                    com.Parameters.AddWithValue("@Page", page);
                    com.Parameters.AddWithValue("@Size", size);
                    try
                    {
                        conn.Open();

                        using (var adapter = new SqlDataAdapter(com))
                        {
                            adapter.Fill(dataTable);
                        }
                    }
                    catch(Exception e)
                    {
                        var a = e;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }

            //Convert datatable to list
            partList = dataTable.AsEnumerable()
                .Select(row => new Part
                {
                    Qpl = Convert.ToBoolean(row["Qpl"]),
                    PartNumber = row["PartNumber"].ToString(),
                    PartName = row["PartName"].ToString(),
                    Revision = row["Revision"].ToString(),
                    ToolDieSetNumber = row["ToolDieSetNumber"].ToString(),
                    OpenPo = Convert.ToBoolean(row["OpenPo"]),
                    Customer = row["Customer"].ToString(),
                    Supplier = row["Supplier"].ToString(),
                    SupplierCodes = row["SupplierCodes"].ToString(),
                    Jurisdiction = row["Jurisdiction"].ToString(),
                    Classification = row["Classification"].ToString(),
                    Ctq = Convert.ToBoolean(row["Ctq"]),
                    QplCreatedByUserId = Convert.ToInt32(row["QplCreatedByUserId"]),
                    QplCreatedDate = Convert.ToDateTime(row["QplCreatedDate"]),
                    QplLastUpdatedByUserId = Convert.ToInt32(row["QplLastUpdatedbyUserId"]),
                    QplLastUpdatedDate = Convert.ToDateTime(row["QplLastUpdatedDate"]),
                    QplExpiresDate = Convert.ToDateTime(row["QplExpiresDate"])
                }).ToList();

            return partList;
        }
    }
}