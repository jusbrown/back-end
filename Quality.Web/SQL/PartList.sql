﻿--DECLARE @Page AS INT;
--DECLARE @Size AS INT;
--SET @Page = 1;
--SET @Size = 20;

WITH PartsList AS
(
    SELECT ROW_NUMBER() OVER(ORDER BY p.PartNumber, qpl.Revision, qpl.ToolDieSetNumber, sc.CompanyName, c.CompanyName) AS RowNum
          ,qpl.IsQualified AS Qpl
          ,p.PartNumber
          ,p.PartName
          ,qpl.Revision
		  ,qpl.ToolDieSetNumber
		  ,qpl.OpenPo
		  ,sc.CompanyName AS Supplier
		  ,c.CompanyName AS Customer
          ,csx.SupplierCodes
		  ,p.Jurisdiction
		  ,p.Classification
		  ,qpl.Ctq
		  ,qpl.CreatedById AS QplCreatedByUserId
		  ,qpl.CreatedDateUtc AS QplCreatedDate
		  ,qpl.LastUpdatedById AS QplLastUpdatedByUserId
		  ,qpl.LastUpdatedDateUtc AS QplLastUpdatedDate
		  ,qpl.ExpirationDateUtc AS QplExpiresDate
      FROM QualifiedPartsLists qpl
	  INNER JOIN Parts p ON qpl.PartId = p.PartId
	  INNER JOIN Companies sc ON qpl.SupplierCompanyId = sc.Id
	  INNER JOIN Companies c ON qpl.CustomerCompanyId = c.Id
	  INNER JOIN CustomerSupplierXref csx ON qpl.SupplierCompanyId = csx.SupplierCompanyId
)

SELECT * 
  FROM PartsList
 WHERE RowNum BETWEEN (@Page - 1) * @Size + 1 
                  AND @Page * @Size;