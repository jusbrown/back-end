﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Quality.Web.Models;
using Quality.Web.Repository;

namespace Quality.Web.Controllers
{
    public class QualityPartController : ApiController
    {
        private IPartRepository _repository;

        //Return a list of parts, first page five items
        public List<Part> Get(int page = 1, int size = 5)
        {
            _repository = new PartRepository();
            return _repository.Get(page, size);
        }
    }
}
