﻿
GO

PRINT N'Update default data in [dbo].[CustomerSupplierXref]...';

GO

-- CustomerSupplierXref
;MERGE INTO dbo.CustomerSupplierXref AS Target
USING (
		select 3, 1, '00093441' union all
		select 3, 2, '00033414' union all
		select 3, 3, ''
) AS Source (CustomerCompanyId, SupplierCompanyId, SupplierCodes)
	ON  Target.CustomerCompanyId = Source.CustomerCompanyId AND
		Target.SupplierCompanyId = Source.SupplierCompanyId
-- update matched rows
WHEN MATCHED THEN UPDATE 
	SET Target.SupplierCodes = Source.SupplierCodes
-- insert new rows
WHEN NOT MATCHED BY TARGET THEN	INSERT 
	VALUES (CustomerCompanyId, SupplierCompanyId, SupplierCodes);

GO